



#ifndef NTC_TEMP_SENSOR_H_
#define NTC_TEMP_SENSOR_H_

#include "stm32f4xx_hal.h"
#include <stdint.h>


/* USER CODE END PD */

typedef struct {
	uint16_t voltage_adc;
	float temp_celsius;
}ADC_struct;

uint8_t temp_measurement(ADC_struct *measure);
void NTC_temp_handling(ADC_struct *measure);



#endif /* INCFILE1_H_ */
