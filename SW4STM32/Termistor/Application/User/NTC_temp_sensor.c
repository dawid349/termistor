
#include "NTC_temp_sensor.h"
#include <math.h>
#include "stm32f4xx_hal.h"
#include <stdint.h>


#define V_REF 3.3				// our reference voltage
#define ADC_RESOLUTION 4096  // resolution of measure
#define R_1 9848			// resistance of resistor which is  connect series to our sensor
#define T_0 298				// T0=25 celsius ----- 298 K
#define RT_0 4700			// resistance of sensor in 25 celsius degree
#define B 3977				// material constant of our sensor


volatile uint16_t voltage[200]={0};				// buffer for adc samples
volatile uint16_t voltage_average=0;			// mean of values in buffer
float scaled_voltage_average=0;					// volatage in volts
float R_T=0;									// current resistance of our sensor
float measured_temp=0;							// temperature mesured
// variables are set global to make debugging easier

uint8_t temp_measurement(ADC_struct *measure)
{
	/*fuction computes the value of temperature based on collect adc samples in adc buffer*/

		uint32_t sum=0;
		for(uint8_t i=0;i<200;i++)
		{
			sum=sum+voltage[i];				// summing up all samples

		}

		voltage_average=sum/200;		// compute mean value
		measure->voltage_adc=voltage_average;		// write our data to stuct
		scaled_voltage_average=voltage_average*V_REF/ADC_RESOLUTION;		// mean voltage in volts
		R_T=(-scaled_voltage_average*R_1)/(scaled_voltage_average-V_REF);	// resistance of our sensor calculated based on voltage divider equation
		measure->temp_celsius=((B*T_0)/(T_0*log(R_T/RT_0)+B))-273;			// write to our structure temperature calculated based on sensor documentation





		return 1;				// return true


}
void NTC_temp_handling(ADC_struct *measure)
{
/* fuction to measured  temperature handle*/




}
